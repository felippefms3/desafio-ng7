//Request da API
let cachedData:any = null;

async function getData() {
  if (cachedData) {
    return cachedData;
  }

  const res = await fetch('https://randomuser.me/api/?results=500')

  if (!res.ok) {
    throw new Error('Failed to fetch data');
  }
 
  cachedData = res.json();
  return cachedData;
}

//Tratamento das informações obtidas
export const LowerAge = async () => {
  const data = await getData();
  const ages = data.results.map((item:any) => item.dob.age);
  return Math.min(...ages);
};

export const HigherAge = async () => {
  const data = await getData();
  const ages = data.results.map((item:any) => item.dob.age);
  return Math.max(...ages);
};

export const MaleGender = async () => {
  const data = await getData();
  const males = data.results.filter((item: any) => item.gender === "male");
  return males.length;
}

export const FemaleGender = async () => {
  const data = await getData();
  const females = data.results.filter((item: any) => item.gender === "female");
  return females.length;
}

export const Country = async () => {
  const data = await getData();
  const countries = data.results.map((item: any) => item.location.country);
  const countryCounts = countries.reduce((counts: any, country: string) => {
    counts[country] = (counts[country] || 0) + 1;
    return counts;
  }, {});
  const mostFrequentCountry = Object.keys(countryCounts).reduce((a, b) =>
    countryCounts[a] > countryCounts[b] ? a : b
  );

  if (countryCounts[mostFrequentCountry] === 1) {
    return "Nenhum País destaque";
  }

  return mostFrequentCountry;
}

export const WomanAbove18 = async () => {
  const data = await getData();
  const womenAbove18 = data.results.filter((item: any) => item.gender === "female" && item.dob.age > 18);
  return womenAbove18.length;
}

export const TopThreeCountries = async () => {
  const data = await getData();
  const countries = data.results.map((item: any) => item.location.country);
  const countryCounts = countries.reduce((counts: any, country: string) => {
    counts[country] = (counts[country] || 0) + 1;
    return counts;
  }, {});

  const top3Countries = Object.keys(countryCounts).sort(
    (a, b) => countryCounts[b] - countryCounts[a]
  );
  return top3Countries.slice(0, 3);
}