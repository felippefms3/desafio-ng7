# Bem vindo(a) ao Dashboard !

# Acesse em: https://felippefms.github.io/
# Ou acesse em: https://gitlab.com/felippefms3/desafio-ng7

Um dashboard para exibir dados de futuros clientes.

![preview1](https://github.com/felippefms/desafio-ng7/blob/main/src/media/imgs/preview1.png?raw=true)
![preview2](https://github.com/felippefms/desafio-ng7/blob/main/src/media/imgs/preview2.png?raw=true)
![preview3](https://github.com/felippefms/desafio-ng7/blob/main/src/media/imgs/preview3.png?raw=true)

***

# Sobre o projeto:

O projeto foi criado para receber dados de uma api e retornar os possíveis clientes além de outras informações e gráficos. Ele foi ótimo para conhecer novas tecnologias e mehorar meus
conceitos diante de tantas mudanças no desenvolvimento front-end, além de me ajudar a entender diversos conceitos que continuarei melhorando todos os dias.

***

# Início do projeto:

Decidi utilizar Nextjs e suas mais recentes features, buscando atender todos os requisitos e utilizar todas as tecnologias solicitadas.
Foi um projeto que me desafiou a utilizar gráficos e tabelas além de ajustar os mesmos para uma boa responsividade.

***

# Tecnologias escolhidas:

Typescript - O Typescript foi utilizado para definir certos tipos de dados utilizados na aplicação para uma melhor tipagem do código.

ReactJs - Atualmente minha paixão tem sido o Nextjs com sua nova documentação, além dos componentes de servidor e de cliente, que já estão mudando a forma como utilizamos JS nos códigos.

SCSS - Decidi utilizar o SCSS para os estilos mais específicos da aplicação e atender os requisitos solicitados, outra alternativa seria o Tailwind, que é recomendado pelo Nextjs, o qual também 
possuo experiência.

ChartJs - Utilizei o ChartJs no projeto para atender os requisitos e renderizar em tela alguns exemplos de gráfico.

GridJs - O GridJs foi utilizado para exibir e filtrar os clientes por idade e por gênero, sendo bem simples de usar, ele se adpta bem ao layout da aplicação sem precisar de muitas configurações.

Bootstrap - O Bootstrap foi utilizado para dar estilos mais simples na aplicação, já possuía experiências anteriores com ele, o que me ajudou a desenvolver os layouts de forma mais rápida.

Nextjs FetchApi - A nova forma de solicitar dados dentro do servidor foi escolhida para testar a economia de dados guardando em cache os resultados da api.

***

# Desenvolvimento do projeto:

Durante o desenrrolar do projeto houve mudanças e decisões que contribuíram para escolhas de melhores tecnologias.
O Nextjs atualmente é a primeira opção para o desenvolvimento em React, já possuo outros projetos, incluindo meu site de portfólio que utilizam ele de forma incrível.
Mantive o código o mais limpo possível, para ter uma boa leitura e entendimento dos componentes.

***

# Funcionalidades do projeto:

O projeto mescla um desgin responsivo seguindo o layout apresentado pelo cliente além de outras funções como:
    . Média de idade entre os possíveis clientes.
    . Gênero dos possíveis clientes.
    . País com mais possíveis clientes.
    . Quantidade de mulheres maiores de idade.
    . Top 3 países com mais posíveis clientes.
    . Tabelas separando os clientes por sexo e idade.

***

# Como inicio o projeto?:

Para iniciar o projeto abra o VSCode, utilize o terminal e escreva "npm install" ou o comando de seu package utilizado "yarn" ou outros.
Após isso utilize o "npm run dev" ou "npm run start" para iniciar o projeto, abra seu navegador e utilize o localhost:3000 ou outra porta referente ao seu terminal.

***

# Dificuldades e desafios:

Grandes mudanças vem acontecendo no desenvolvimento front-end, principalmente com o React, o que tornou um desafio maior utilizar as novas tecnologias para um projeto importante como esse.
Os componentes de servidor e a FetchAPI são ótimos, mas ainda muito recentes, o que dificultou o processo do desenvolvimento, pois nem todas as tecnologias estão acompanhando as mudanças.

***

# Agradecimentos:

Fico feliz em aceitar um desafio desse em um momento muito agitado da minha vida, acabei de me formar na faculdade e por isso meu estágio acabou, estar de volta no mercado de trabalho
enquanto desenvolvo usando tecnologias novas está sendo um baita desafio, mas fiquei feliz e determinado a melhorar.

# Obrigado por ler até aqui !!!