import './globals.css'
import 'bootstrap/dist/css/bootstrap.css';

export const metadata = {
  title: 'desafio-ng7',
  description: 'desafio-ng7',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="pt-br">
      <meta name="viewport" content="width=device-width, initial-scale=1"></meta>
      <body>{children}</body>
    </html>
  )
}