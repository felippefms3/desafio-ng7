import Menu from '@/components/Menu/menu'
import DashboardSection from '@/components/DashboardSection/dashboardsection'

import './style.scss'

export default function Homepage(){
    return(
        <div className='homepageWrapper flex-md-row'>
            <Menu></Menu>
            {/* @ts-expect-error Async Server Component*/}
            <DashboardSection></DashboardSection>
        </div>
    )
}