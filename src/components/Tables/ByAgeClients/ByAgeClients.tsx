'use client'

import { Grid } from "gridjs";
import { useRef, useEffect } from "react";
import "gridjs/dist/theme/mermaid.css";

export default function ByAgeClients() {
  const wrapperRef = useRef(null);

  const grid = new Grid({
    columns: ['Nome', 'Idade'],
    sort: true,
    pagination: {
      limit: 10
    },
    server: {
      url: 'https://randomuser.me/api/?results=500',
      then: data => data.results.map((item: { name: { first: string; }; dob: { age: number; }; }) => [item.name.first, item.dob.age])
    },
    style: {
      table: {
        
      },
      th: {
        'background-color': 'rgba(0, 0, 0, 0.1)',
        color: '#6C757D',
        'border-bottom': '3px solid #ccc',
        'text-align': 'center'
      },
      td: {
        'text-align': 'center'
      }
    }
  });

  useEffect(() => {
    if (wrapperRef.current) {
      grid.render(wrapperRef.current);
    }
  }, []);

  return <div ref={wrapperRef} />;
}