import NumbersSection from '@/components/NumbersSection/numberssection'
import DoughnutChartBox from '../Charts/DoughnutChartBox/DoughnutChartBox'
import LineChartBox from '../Charts/LineChartBox/LineChartBox'
import BarChartBox from '../Charts/BarChartBox/BarChartBox'
import ByAgeClients from '../Tables/ByAgeClients/ByAgeClients'
import ByGenderClients from '../Tables/ByGenderClients/ByGenderClients'
import './style.scss'

export default async function DashboardSection(){
    return(
        <div className='dashboardsection p-3 pt-md-3 p-md-5'>
            <div className='d-flex flex-column flex-md-row justify-content-between fw-bold text-secondary my-4'>
                <p className='fs-3 align-self-center'>Dashboard</p>
                <div className='d-flex justify-content-center justify-content-md-end column-gap-2'>
                    <button type="button" className='btn rounded-pill bg-custom-grey fw-bold'>Nome Empresa</button>
                    <button type="button" className='btn rounded-pill text-black bg-custom-white fw-bold'>2022 - 2023</button>
                </div>
            </div>
            {/* @ts-expect-error Async Server Component*/}
            <NumbersSection></NumbersSection>
            
            <div className='d-flex flex-column flex-md-row justify-content-around'>
                <div>
                    <LineChartBox></LineChartBox>
                </div>
                <div>
                    <BarChartBox></BarChartBox>
                </div>
                <div>
                    <DoughnutChartBox></DoughnutChartBox>
                </div>
            </div>

            <div className='d-flex flex-column flex-md-row align-items-center justify-content-center'>
                <ByAgeClients></ByAgeClients>
                <ByGenderClients></ByGenderClients>
            </div>
        </div>
    )
}