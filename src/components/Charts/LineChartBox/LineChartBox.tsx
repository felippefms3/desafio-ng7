import { LineChart } from "./LineChart/LineChart";

export default function DoughnutChartBox() {
  return (
    <div className="d-flex flex-md-row flex-md-colum justify-content-center align-items-center bg-white p-2 my-4 text-secondary fw-bold">
      <div className="pe-4">
        <p>Exemplo Gráfico</p>
      </div>
      <LineChart></LineChart>
    </div>
  );
}