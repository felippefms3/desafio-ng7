'use client'
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
  } from 'chart.js';

  import { Line } from 'react-chartjs-2';
  
  ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend
  );

export const options = {
  plugins: {
    legend: {
      display: false,
    },
    doughnutLabel: {
      labels: [],
    },
  },
  responsive:true,
  maintainAspectRatio:false,
  
};

export const data = {
    labels: ['2018','2019','2020','2021','2022','2023'],
    datasets: [
      {
        label: 'Exemplo',
        data: [100,220,290,320,450,500],
        backgroundColor: [
          '#CFCFCF',
          'rgb(131, 131, 131)'
        ],
        borderColor: [
          '#909DAD',
        ],
      },
    ],
  };

export function LineChart() {
  return (
    <div className='d-flex container.fluid flex-column flex-md-row'>
        <Line data={data} options={options}/>
    </div>
  )
}