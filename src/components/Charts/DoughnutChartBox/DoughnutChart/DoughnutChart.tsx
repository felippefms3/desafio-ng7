'use client'
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from 'chart.js';
import { Doughnut } from 'react-chartjs-2';

ChartJS.register(ArcElement, Tooltip, Legend);

export const options = {
  plugins: {
    legend: {
      display: false,
    },
    doughnutLabel: {
      labels: [],
    },
  },
  responsive:true,
  maintainAspectRatio:false,
  
};

export const data = {
    labels: [],
    datasets: [
      {
        label: 'Exemplo',
        data: [30,70],
        backgroundColor: [
          '#CFCFCF',
          '#909DAD'
        ],
        borderColor: [
          '#909DAD',
        ],
      },
    ],
  };

export function DoughnutChart() {
  return (
    <div className='d-flex container.fluid flex-column flex-md-row'>
        <Doughnut data={data} options={options}/>
    </div>
  )
}