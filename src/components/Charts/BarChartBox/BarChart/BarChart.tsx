'use client'
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
import { Bar } from 'react-chartjs-2';

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);

export const options = {
    responsive: true,
    plugins: {
      legend: {
        position: 'top' as const,
      },
      title: {
        display: false,
      },
    },
  };

const labels = ['2018','2019','2020','2021','2022','2023'];

export const data = {
    labels,
    datasets: [
      {
        label: 'Exemplo',
        data: [100,220,290,320,450,500],
        backgroundColor: [
          '#CFCFCF',
          '#909DAD'
        ],
      }
    ],
  };

export function BarChart() {
    return(
        <div className='d-flex container.fluid flex-column flex-md-row'>
            <Bar data={data} options={options}/>
        </div>
    )
}