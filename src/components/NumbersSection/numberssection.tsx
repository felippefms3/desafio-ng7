import InfoBox from '../Infobox/infobox'

import { LowerAge, HigherAge, MaleGender, FemaleGender, Country, WomanAbove18, TopThreeCountries } from '../../../api/apiRequest'

import './style.scss'

import ageimage from '../../media/imgs/age.png'
import genderimage from '../../media/imgs/gender.png'
import womanabove18image from '../../media/imgs/plus18woman.png'
import countriesimage from '../../media/imgs/countries.png'
import topcountriesimage from '../../media/imgs/top3.png'

export default async function NumbersSection(){
    //idade dos clientes
    const lowerAgeValue = await LowerAge();
    const higherAgeValue = await HigherAge();
    const agesResult = `${lowerAgeValue} ~ ${higherAgeValue} Anos`;

    //genero dos clientes
    const malesValue = await MaleGender();
    const femalesValue = await FemaleGender();
    const genderResult = `${malesValue} Homens ~ ${femalesValue} Mulheres`;

    //país com mais clientes
    const contryValue = await Country();
    
    //mulheres acima de 18 anos
    const womanAbove18Value = await WomanAbove18();

    //Top 3 países com mais chances de clientes
    const top3CountriesValue = await TopThreeCountries();
    const top3CountriesResult = `${top3CountriesValue[0]} ~ ${top3CountriesValue[1]} ~ ${top3CountriesValue[2]}`;

    return(
        <div className='d-flex flex-wrap flex-column flex-xxl-row justify-content-between'>
            <InfoBox imagesrc={ageimage} text={'Idade dos clientes'} specificdata={agesResult}></InfoBox>
            <InfoBox imagesrc={genderimage} text={'Gênero dos clientes'} specificdata={genderResult}></InfoBox>
            <InfoBox imagesrc={countriesimage} text={'País com mais chance de clientes'} specificdata={contryValue}></InfoBox>
            <InfoBox imagesrc={womanabove18image} text={'Qnt. Mulheres maiores de idade'} specificdata={womanAbove18Value}></InfoBox>
            <InfoBox imagesrc={topcountriesimage} text={'Top 3 Países com mais clientes'} specificdata={top3CountriesResult}></InfoBox>
        </div>
    )
}