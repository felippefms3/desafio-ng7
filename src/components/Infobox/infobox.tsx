import Image from 'next/image'

import './style.scss'

export default function InfoBox(props:any){
    return(
        <div className='d-flex justify-content-start align-items-center text-secondary fw-bold py-2 px-2 bg-white'>
            <div className='d-flex me-4'>
                <Image src={props.imagesrc} alt='Info'></Image>
            </div>
            <div className='d-flex flex-column'>
                <p>{props.text}</p>
                <p>{props.specificdata}</p>
            </div>
        </div>
    )
}