import Image from 'next/image'
import Link from 'next/link'
import './style.scss'

import dashboardimg from '../../media/imgs/dashboard.png'
import percentimg from '../../media/imgs/percent.png'
import graphicsimg from '../../media/imgs/graphics.png'

export default function Menu(){
    return(
        <nav className='navbar flex-md-column justify-content-md-start fw-semibold'>
            <ul className='navbar-nav'>
                <div className='logo'>
                    <p>LOGO</p>
                </div>
                <Link href="/" className='nav-item flex-md-column mb-md-4 pe-md-2 ps-md-2'>
                    <li className='d-flex flex-column justify-content-center align-items-center'>
                        <Image src={dashboardimg} alt='Dashboard' className='menuimg'></Image>
                        Dashboard
                    </li>
                </Link>
                <Link href="/" className='nav-item flex-md-column mb-md-4 pe-md-2 ps-md-2'>
                    <li className='d-flex flex-column justify-content-center align-items-center'>
                        <Image src={percentimg} alt='Dashboard' className='menuimg'></Image>
                        Porcentagens
                    </li>
                </Link>
                <Link href="/" className='nav-item flex-md-column mb-md-4 pe-md-2 ps-md-2'>
                    <li className='d-flex flex-column justify-content-center align-items-center'>
                        <Image src={graphicsimg} alt='Dashboard' className='menuimg'></Image>
                        Gráficos
                    </li>
                </Link>
            </ul>
        </nav>
    )
}